<?php

Route::get('categorie', 'TileController@displayCategories');
Route::get('categorie/{name}', 'TileController@displayCatTiles');
Route::get('tegel/{name}', 'TileController@displayTile');

/** Admin **/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function()
{
    Route::group(['middleware' => 'guest'], function()
    {
       
    });

    Route::group(['middleware' => 'auth'], function()
    {
        
		//ROUTES: Tiles Main
		Route::get('tiles', ['as' => 'tiles', 'uses' => 'TileController@tiles']);
		Route::get('tiles/c/{id}', ['as' => 'tilesLimit', 'uses' => 'TileController@tilesLimit']);
		
		Route::get('tiles/create', ['as' => 'getTileCreate', 'uses' => 'TileController@getTileCreate']);
		Route::post('tiles/create', ['as' => 'tileAdd', 'uses' => 'TileController@tileAdd']);
		
		Route::get('tiles/images/{id}', ['as' => 'getTileImages', 'uses' => 'TileController@getTileImages']);
		
		Route::post('tiles/upload', ['as' => 'imageUp', 'uses' => 'TileController@imageUp']);
		Route::post('tiles/iorder', ['as' => 'imageOrder', 'uses' => 'TileController@imageOrder']);
		
		Route::get('tiles/edit/{id}', ['as' => 'getTileEdit', 'uses' => 'TileController@getTileEdit']);
		Route::post('tiles/edit', ['as' => 'tilesEdit', 'uses' => 'TileController@tilesEdit']);
		Route::get('tiles/delete/{id}', ['as' => 'tilesDelete', 'uses' => 'TileController@tilesDelete']);
		
		//ROUTES: Tile Categories
		Route::get('tile_categories', ['as' => 'tile_categories', 'uses' => 'TileController@categories']);
		
		Route::get('tile_categories/create', ['as' => 'getCatCreate', 'uses' => 'TileController@getCatCreate']);
		Route::post('tile_categories/create', ['as' => 'categoryAdd', 'uses' => 'TileController@categoryAdd']);
		
		Route::get('tile_categories/edit/{id}', ['as' => 'getCatEdit', 'uses' => 'TileController@getCatEdit']);
		Route::post('tile_categories/edit', ['as' => 'categoryEdit', 'uses' => 'TileController@categoryEdit']);
		Route::get('tile_categories/delete/{id}', ['as' => 'categoryDelete', 'uses' => 'TileController@categoryDelete']);
		
    });
});
/** End Admin */

Route::get('{main}/{sub}', 'TileController@displayCatSubTiles');