<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Model\Admin\Tiles;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\CreateAccountRequest;
use App\Http\Requests\Admin\EditAccountRequest;
use App\Models\Admin\Admin;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Repositories\AdminInterface;
use Laracasts\Flash\Flash;

class TileController extends Controller
{
    public function tiles()
    {
		//Get the list of the tiles and categories
		$tiles = DB::table('tiles')->select('id', 'cid', 'name', 'price', 'notes', 'description', 'images')->orderBy('id', 'desc')->get();
		$cats = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', '')->orderBy('id', 'desc')->get();
		//Initialize and array for category list
		$cat_list = categoryArrayList($cats);
		
		//Initialize variables
		$cn = "Alle spullen";
		$in = "tiles/c/";
		$all = "";
		//Return to view with the data.
        return view('admin.tiles.tiles', compact('tiles', 'cats', 'cn', 'in', 'all', 'cat_list'));
    }
	
	public function tilesLimit($id)
	{
		//Form the id to mirror database
		$tid = "|" . $id . "|";
		//get tile categories that are not this category
		$cats = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', '')->where('id', '<>', $id)->orderBy('id', 'desc')->get();
		//Initialize and array for category list
		$cat_list = categoryArrayList($cats);
		
		//get category info
		$get_cn = DB::table('tile_categories')->select('name', 'pid')->where('id', '=', $id)->get();
		//initialize parent variable to be added before the name of the category if it has a parent
		$parent = "";
		//if it has a parent get the name and add it to the parent variable
		if($get_cn[0]->pid != ''){
			$get_p = DB::table('tile_categories')->select('name')->where('id', '=', $get_cn[0]->pid)->get();
			$parent = $get_p[0]->name . " - ";
		}
		//Initialize variables
		$cn = $parent . $get_cn[0]->name;
		$in = "";
		$all = "<li><a href=\"../\">Alle spullen</a></li>";
		
		//Get all 
		$tiles = DB::table('tiles')->select('id', 'cid', 'name', 'price', 'notes', 'description', 'images')->where('cid', 'LIKE', '%' . $tid . '%')->orderBy('id', 'desc')->get();
		
		//Return to view with the data.
        return view('admin.tiles.tiles', compact('tiles', 'cats', 'cn', 'in', 'all', 'cat_list'));
	}
	
	public function getCatCreate(){
		//get categories list
		$cats = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', '')->orderBy('id', 'desc')->get();
		//return to view with data.
		return view('admin.tiles.makecat', compact('cats'));
	}
	
	public function getTileCreate(){
		
		//get the list of categories
		$cats = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', '')->orderBy('id', 'asc')->get();
		//Initialize and array for category list
		$cat_list = categoryArrayList($cats);
		//return to view with data.
		return view('admin.tiles.maketile', compact('cats', 'cat_list'));
	}
	
	public function getTileImages($id){
		//initialize tile with the id to pass to view later.
		$tile = $id;
		//get the images string from the database
		$img = DB::table('tiles')->select('images', 'id')->where('id', '=', $tile)->orderBy('id', 'desc')->get();
		//split the image string if its not empty
		if($img[0]->images != ""){
			$images = explode("|", $img[0]->images);
		}
		else{
			$images = "";
		}
		//return to view with data.
		return view('admin.tiles.images', compact('tile', 'images'));
	}
	
	public function imageUp(Request $request){
		// If new images is added to the tile
		if($request->hasFile('image')){
			//initialize image count -1 for count starts at 0;
			$total = $request->get('tots') - 1;
			$count = 0;
			//initialize image string
			$images = "";
			//while loop for each image to order in string properly for already existing if order has been chagned
			while($count <= $total){
				$temp = $request->get($count) . "|";
				if($temp != "|"){
					$images .= $temp;
				}
				$count++;
			}
			
			//Asign the image input to a variable
			$file = $request->file('image');
			//Save location variable
			$destinationPath = 'source/offline/images/tiles';
			//Generate a timestamp to use as a randomizer to the icon name
			$time = \Carbon\Carbon::now()->timestamp;
			//Asign the name of the file and the extension to a variable
			$name = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			//Hash the combination of the time and name to generate a random string name
			$hash = hash('ripemd160', $time . $name);
			//add the extension to the name and update the out variable
			$out = $hash . "." . $ext;
			//Move the file to the save location
			$file->move($destinationPath,$out);
			//append to images string.
			$images .= $out;
			//update 
			DB::table('tiles')->where('id', '=', $request->id)->update(['images' => $images]);
			//return back.
			return \Redirect::back()->with('message', 'Afbeeldingen zijn toegevoegd');
		}
		else{
			// otherwise return back
			return \Redirect::back();
		}
		
	}
	
	public function imageOrder(Request $request){
		//total count of images -1 for the count starts at 0
		$total = $request->get('tots') - 1;
		$count = 0;
		//initialize images string.
		$images = "";
		//while loop to reorder with the new order
		while($count <= $total){
			
			$temp = $request->get($count) . "|";
			if($temp != "|"){
				$images .= $temp;
			}
			$count++;
		}
		$images = rtrim($images,'|');
		//update the order of the images
		DB::table('tiles')->where('id', '=', $request->id)->update(['images' => $images]);
		//return back to the previous view
		return \Redirect::back()->with('message', 'Beeldvolgorde opgeslagen');
	}
	
	public function getCatEdit($id){
		//get the category data and a list with all the categories
		$category = DB::table('tile_categories')->select('id', 'name', 'description', 'icon', 'pid')->where('id', '=', $id)->get();
		$cats = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', '')->orderBy('id', 'desc')->get();
		//return to view with data.
		return view('admin.tiles.editcat', compact('category', 'cats'));
	}
	
	public function getTileEdit($id){
		
		//get the tile data and the categories as a list
		$tiles = DB::table('tiles')->select('id', 'cid', 'name', 'price', 'notes', 'description')->where('id', '=', $id)->get();
		$cats = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', '')->orderBy('id', 'asc')->get();
		
		//Initialize and array for category list
		$cat_list = categoryArrayList($cats);
		
		//initialize variables
		$cs = substr($tiles[0]->cid, 1, -1);
		$cs = str_replace("|",",",$cs);
		
		//return to view with data.
		return view('admin.tiles.edittile', compact('tiles', 'cats', 'cs', 'cat_list'));
        
	}

    public function categories(){
		//get a list with the categories information
        $categories = DB::table('tile_categories')->select('id', 'name', 'description')->where('pid', '=', '')->orderBy('id', 'desc')->get();
		//Initialize and array for category list
		$cat_list = categoryArrayList($categories);
		//return to view with the data.
        return view('admin.tiles.categories', compact('categories', 'cat_list'));
    }
	
	public function categoryAdd(Requests\Admin\CategoryAddRequest $request){
		$out = "";
		if($request->hasFile('caticon')){
			//Asign the icon input to a variable
			$file = $request->file('caticon');
			//Save location variable
			$destinationPath = 'source/offline/images/icons';
			//Generate a timestamp to use as a randomizer to the icon name
			$time = \Carbon\Carbon::now()->timestamp;
			//Asign the name of the file and the extension to a variable
			$name = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			//Hash the combination of the time and name to generate a random string name
			$hash = hash('ripemd160', $time . $name);
			//add the extension to the name and update the out variable
			$out = $hash . "." . $ext;
			//Move the file to the save location
			$file->move($destinationPath,$out);
		}
		
		DB::table('tile_categories')->insert(
			['name' => $request->get('name'), 'description' => $request->get('description'), 'icon' => $out, 'pid' => $request->get('parent')]
		);
		//insert into the database the category data.
		return \Redirect::route('tile_categories')->with('message', 'Categorie < '. $request->get('name') .' > is toegevoegd');
	}
	
	public function tileAdd(Requests\Admin\TileAddRequest $request){
		//Initialize the out variable.
		$out = "";
		
		//If the tile has an image
		if($request->hasFile('image')){
			//Asign the image input to a variable
			$file = $request->file('image');
			//Save location variable
			$destinationPath = 'source/offline/images/tiles';
			//Generate a timestamp to use as a randomizer to the icon name
			$time = \Carbon\Carbon::now()->timestamp;
			//Asign the name of the file and the extension to a variable
			$name = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			//Hash the combination of the time and name to generate a random string name
			$hash = hash('ripemd160', $time . $name);
			//add the extension to the name and update the out variable
			$out = $hash . "." . $ext;
			//Move the file to the save location
			$file->move($destinationPath,$out);
		}
		
		if($request->get('category') == ""){
			//if there is no asigned category for the tile put it the Uncategorized ( 0 ZERO ) category.
			$cids = "|0|";
		}
		else{
			//Replace the coma in the string of category ids
			$cs = str_replace(",","|",$request->get('category'));
			//Form the database proper start and end for the id string.
			$cids = "|" . $cs . "|";
		}
		//insert into the database the tile data.
		DB::table('tiles')->insert(
			['name' => $request->get('name'), 'price' => $request->get('price'), 'notes' => $request->get('notes'), 'description' => $request->get('description'), 'cid' => $cids, 'images' => $out]
		);
		//return to tiles view
		return \Redirect::route('tiles')->with('message', 'Tegel < '. $request->get('name') .' > is toegevoegd');
	}
	
	public function categoryEdit(Requests\Admin\CategoryEditRequest $request){
		//Initialize out variable.
		$out = "";
		//Check if the category edit has its icon changed otherwise just update the data without the icon.
		if($request->hasFile('caticon')){
			//Asign the icon to a variable.
			$file = $request->file('caticon');
			//Save location for the icons
			$destinationPath = 'source/offline/images/icons';
			//Generate a timestamp to use as a randomizer to the icon name
			$time = \Carbon\Carbon::now()->timestamp;
			//Asign the name of the file and the extension to a variable
			$name = $file->getClientOriginalName();
			$ext = $file->getClientOriginalExtension();
			//Hash the combination of the time and name to generate a random string name
			$hash = hash('ripemd160', $time . $name);
			//add the extension to the name and update the out variable
			$out = $hash . "." . $ext;
			//Move the file to the save path
			$file->move($destinationPath,$out);
			//Update the category with the new data.
			DB::table('tile_categories')->where('id', '=', $request->id)->update(['name' => $request->name, 'description' => $request->description, 'icon' => $out, 'pid' => $request->par]);
		}
		else
		{
			//Update the category with the new data.
			DB::table('tile_categories')->where('id', $request->id)->update(['name' => $request->name, 'description' => $request->description, 'pid' => $request->par]);
		}
		//return to tile category list
		return \Redirect::route('tile_categories')->with('message', 'Categorie < '. $request->get('name') .' > is bewerkt');
	}
	
	public function tilesEdit(Request $request){
		//Form the tile category id
		$cs = "|" . $request->get('category') . "|";
		$cs = str_replace(",","|",$cs);
		
		//Update the tile with the new edited data
		DB::table('tiles')->where('id', $request->id)->update(['name' => $request->name, 'cid' => $cs, 'price' => $request->price, 'notes' => $request->notes, 'description' => $request->description]);
		//Return to tile list
		return \Redirect::route('tiles')->with('message', 'Tegel < '. $request->get('name') .' > is bewerkt');
	}
	
	public function categoryDelete($id){
		
		//Form the category id to compare in the database.
		$tid = "|" . $id . "|";
		//Get the count of items that are in the category.
		$tiles = DB::table('tiles')->select('id', 'cid', 'name', 'price', 'notes', 'description', 'images')->where('cid', 'LIKE', '%' . $tid . '%')->count();
		
		if($tiles > 0){
			//Return to tile/items list if there are items in the category present
			return \Redirect::route('tile_categories')->with('error', 'Kan een categorie met items niet verwijderen');
		}
		else{
			//Delete the tile category from the database.
			DB::table('tile_categories')->where('id', '=', $id)->delete();
			//return to tile category list
			return \Redirect::route('tile_categories')->with('message', 'Categorie is verwijderde');
		}
	}
	
	public function tilesDelete($id){
		//Delete tile from the database.
		DB::table('tiles')->where('id', '=', $id)->delete();
		//Return to tile/items list.
		return \Redirect::route('tiles')->with('message', 'Tegel is verwijderde');
	}
	
	public function categoryArrayList($cats){
		//Initialize and array for category list
		$cat_list = array();
		//Foreach category pass it into the array
		foreach($cats as $cat){
			$cat_list[] = array("id"=>$cat->id, "name"=>$cat->name);
			//Check if the category has a sub category and pass it into the array also under the main category
			$sub = DB::table('tile_categories')->select('id', 'name')->where('pid', '=', $cat->id)->orderBy('id', 'desc')->get();
			if(!empty($sub)){
				foreach($sub as $s){
					$cat_list[] = array("id"=>$s->id, "name"=>"<i> - " . $s->name . "</i>");
				}
			}
		}
		//return the array
		return $cat_list;
	}
	
	public function ajxCat($val){
		$ids = explode(",", $val);
		$ret = "";
		
		foreach($ids as $id){
			$cat = DB::table('tile_categories')->select('name', 'pid')->where('id', '=', $id)->get();
			if(!empty($cat)){
				$parent = "";
				if($cat[0]->pid != ""){
					$par = DB::table('tile_categories')->select('name')->where('id', '=', $cat[0]->pid)->get();
					$parent = $par[0]->name . " - ";
				}
				$ret .= "<span class='badge' style='margin-right:5px;margin-bottom:5px;'>". $parent . $cat[0]->name ."</span>";
			}
			else{
				$ret .= "<span class='badge' style='margin-right:5px;margin-bottom:5px;background:#df4a58;'>BESTAAT NIET</span>";
			}
		}
		
		
		return $ret;
	}

}
