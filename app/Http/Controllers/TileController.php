<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Response;

class TileController extends Controller
{
   
	public function displayCategories(){
		//Get ALL categories to display and return them to view
		$categories = DB::table('tile_categories')->select('id', 'name', 'icon' )->orderBy('id', 'desc')->get();
		return view('tiles.categorie', compact('categories'));
	}
	
	public function displayCatTiles($name){
		//Replace/Fix the url to spaces instead of dashes
		$act_name = str_replace("-"," ",$name);
		//Get the id and description of the category based on the name
		$get_id = DB::table('tile_categories')->select('id', 'description')->where('name', '=', $act_name)->get();
		//variables to hold data to pass to view or use later
		$cid = $get_id[0]->id;
		$desc = $get_id[0]->description;
		
		//Get the tiles/tiems that belong to the category
		$tiles = DB::table('tiles')->select('id', 'name', 'images' )->where('cid', 'LIKE', '|' . $cid . '|')->get();
		//Return to view with data.
		return view('tiles.tiles', compact('tiles', 'desc', 'act_name'));
	}
	
	public function displayCatSubTiles($main, $sub){
		//Replace/Fix the url to spaces instead of dashes for both main and sub category
		$act_main = str_replace("-"," ",$main);
		$act_sub = str_replace("-"," ",$sub);
		//Get the main category id and sub category id and description based on the name 
		$main_id = DB::table('tile_categories')->select('id')->where('name', '=', $act_main)->get();
		$sub_id = DB::table('tile_categories')->select('id', 'description')->where('name', '=', $act_sub)->get();
		//variables to hold data to pass to view or use later
		$mcid = $main_id[0]->id;
		$scid = $sub_id[0]->id;
		$desc = $sub_id[0]->description;
		
		//get all tiles/items in the sub category
		$tiles = DB::table('tiles')->select('id', 'name', 'images' )->where('cid', 'LIKE', '|' . $scid . '|')->get();
		
		//set act_name variable to be the same as the act_sub variable, less hassle over the the view to display the sub category name.
		$act_name = $act_sub;
		//Return to view with data.
		return view('tiles.tiles', compact('tiles', 'desc', 'act_name'));
	}
	
	public function displayTile($name){
		//Replace/Fix the url to spaces isntead of dashes for the tile/item name
		$act_name = str_replace("-"," ",$name);
		//Get the information needed for the the tile/item.
		$tile = DB::table('tiles')->select('id', 'name', 'price', 'notes', 'description','images' )->where('name', '=', $act_name)->get();
		//Return to view with data.
		return view('tiles.single', compact('tile', 'act_name'));
	}
}
