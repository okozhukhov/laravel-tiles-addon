@extends('admin.layouts.general')

@section('content')
    <h3 class="page-header page-header-top">Tegels Categorieën</h1>
        <div class="row">
            <div class="col-md-12">
				@if (Session::has('message'))
				   <div class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ Session::get('message') }}</div>
				@endif
				@if (Session::has('error'))
				   <div class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ Session::get('error') }}</div>
				@endif
				<div class="row">
					<div class="col-sm-12">
						<a href="{{ URL::action('Admin\TileController@getCatCreate') }}" class="btn btn-sm btn-success">Categorie toevoegen</a>
					</div>
				</div>
				<br/>
					<table class="table table-bordered">
						<thead>
						<tr>
							<th class="cell-small text-center">ID</th>
							<th>Categorie</th>
							<th class="cell-small text-center">Acties</th>
						</tr>
						</thead>

						<tbody>

						<?php foreach($cat_list as $cat){ ?>

							<tr>
								<td class="text-center"><?php echo "<i>" . $cat['id'] . "</i>"; ?></td>
								<td><?php echo "<strong>" . $cat['name'] . "</strong>" ?></td>
								<td class="text-center">
								<?php if($cat['id'] != "0") { ?>
									<div class="btn-group">
										<a href="tile_categories/edit/<?php echo $cat['id']; ?>" data-toggle="tooltip" title="Bewerken" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
										<a href="tile_categories/delete/<?php echo $cat['id']; ?>" data-toggle="tooltip" title="Verwijderen" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
									</div>
								<?php } ?>
								</td>
							</tr>

						<?php } //print_r($cat_list); ?>
						
						

						</tbody>
					</table>
				
            </div>
        </div>
@endsection