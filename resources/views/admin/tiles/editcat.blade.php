@extends('admin.layouts.general')

@section('content')
    <h3 class="page-header page-header-top">Tegels Categorieën Bewerk</h1>
        <div class="row">
            <div class="col-md-12">
				
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
					
					{!! Form::open(array('route' => 'categoryEdit', 'class' => 'form', 'enctype' => 'multipart/form-data')) !!}
					
					{!! Form::hidden('id',  $category[0]->id) !!}

					<div class="form-group">
						{!! Form::label('Categorie naam') !!}
						{!! Form::text('name',  $category[0]->name, 
							array('required', 
								  'class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('Categorie korte beschrijving') !!}
						{!! Form::textarea('description',  $category[0]->description, 
							array( 'class' => 'form-control input-sm ckeditor')) !!}
					</div>
					<div class="form-group" style="float:left;width:49%;">
						<?php if($category[0]->icon != ""){ ?>
						<img src="/source/offline/images/icons/<?php echo $category[0]->icon; ?>" style="float:left;height:100px;width:100px;border:3px solid #ccc;margin-right:10px;" />
						<?php } ?>
						{!! Form::label('Categorie pictogram ( Selecteer geen nieuwe als u deze niet wilt wijzigen )') !!}
						{!! Form::file('caticon') !!}
					</div>
					<div class="form-group" style="float:right;width:49%;">
						{!! Form::label('Bovenliggende categorie') !!}
						<select style="width:70%;padding:7px 12px;" id="csel" name="par">
						<option value="" <?php if($category[0]->pid == ""){ echo "selected"; } ?>>Geen</option>
						<?php foreach($cats as $c){ ?>
							<option value="<?php echo $c->id; ?>" <?php if($c->id == $category[0]->pid){ echo "selected"; } ?>><?php echo $c->name; ?></option>
						<?php } ?>
						</select> 
					</div>
					<div style="clear:both;"></div>
					<div class="form-group">
						{!! Form::submit('Opslaan', 
						  array('class'=>'btn btn-primary')) !!}
					</div>
					{!! Form::close() !!}
					
					
            </div>
        </div>
@endsection