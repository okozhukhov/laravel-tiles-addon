@extends('admin.layouts.general')

@section('content')
    <h3 class="page-header page-header-top">Tegels Afbeeldingen</h1>
        <div class="row">
            <div class="col-md-12" style="border-bottom:1px solid #ddd; margin-bottom:40px;">
				
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
					
					{!! Form::open(array('route' => 'imageUp', 'class' => 'form', 'enctype' => 'multipart/form-data')) !!}
					
					{!! Form::hidden('id',  $tile) !!}
					
					
					<?php
					$i = 0;
					if($images != ""){
					foreach($images as $img){
					?>
					{!! Form::hidden($i,  $img) !!}
					<?php $i++; } }	?>
					{!! Form::hidden('tots',  $i) !!}
					<div class="form-group">
						{!! Form::label('Voeg afbeeldingen toe') !!}
						{!! Form::file('image') !!}
					</div>
					<div class="form-group">
						{!! Form::submit('Uploaden', 
						  array('class'=>'btn btn-primary')) !!}
					</div>
					{!! Form::close() !!}
					
					
            </div>
			<div class="col-md-12">
				
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
					
					{!! Form::open(array('route' => 'imageOrder', 'class' => 'form', 'enctype' => 'multipart/form-data')) !!}
					
					{!! Form::hidden('id',  $tile) !!}
					
					
					<?php
					$x = 0;
					if($images != ""){
					foreach($images as $img){
					?>
					{!! Form::hidden($x,  $img) !!}
					<?php $x++;				}	} ?>

					{!! Form::hidden('tots',  $x) !!}
					<div class="form-group">
						{!! Form::submit('Bewaar bestelling / verhuizingen', 
						  array('class'=>'btn btn-primary')) !!}
					</div>
					{!! Form::close() !!}
					
					
            </div>
			<?php
			$o = 0;
			if($images != ""){
			foreach($images as $img){ 
			?>
			
				<div class="col-md-2"><a class="left" id="<?php echo $o; ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><a class="delete" id="<?php echo $o; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a><a class="right" id="<?php echo $o; ?>" style="float:right;"><i class="fa fa-arrow-right" aria-hidden="true"></i></a><img src="/source/offline/images/tiles/<?php echo $img; ?>" style="width:100%;height:100%;" /></div>
			
			<?php $o++; }} ?>
        </div>
		
		<script>
			$(".left").click(function(){
				var pid = $(this).attr('id');
				var cid = parseFloat(pid) - 1;
				if(pid > 0){
					var sin = $( "input[name*='"+ pid +"']" ).val();
					var img1 = $("#"+pid).parent().find('img').attr('src');
					var img2 = $("#"+cid).parent().find('img').attr('src');
					
					$( "input[name*='"+ pid +"']" ).val($( "input[name*='"+ cid +"']" ).val());
					$( "input[name*='"+ cid +"']" ).val(sin)
					
					$("#"+pid).parent().find('img').attr('src', img2);
					$("#"+cid).parent().find('img').attr('src', img1);
				}
				else
				{
					alert("Deze foto is als eerste");
				}
			});
			
			$(".right").click(function(){
				
				var pid = $(this).attr('id');
				var cid = parseFloat(pid) + 1;
				if(cid < <?php echo $i; ?>){
					var sin = $( "input[name*='"+ pid +"']" ).val();
					var img1 = $("#"+pid).parent().find('img').attr('src');
					var img2 = $("#"+cid).parent().find('img').attr('src');
					
					$( "input[name*='"+ pid +"']" ).val($( "input[name*='"+ cid +"']" ).val());
					$( "input[name*='"+ cid +"']" ).val(sin)
					
					$("#"+pid).parent().find('img').attr('src', img2);
					$("#"+cid).parent().find('img').attr('src', img1);
				}
				else
				{
					alert("Deze foto is voor het laatst");
				}
			});
			
			$(".delete").click(function(){
				
				var pid = $(this).attr('id');
					var sin = $( "input[name*='"+ pid +"']" ).val();
					
					$( "input[name*='"+ pid +"']" ).val("");
					
					$("#"+pid).parent().remove();
				
			});
		</script>
@endsection