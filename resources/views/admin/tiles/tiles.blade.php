@extends('admin.layouts.general')

@section('content')
    <h3 class="page-header page-header-top">Tegels Lijst</h1>
        <div class="row">
			<div class="col-md-12">
				
				@if (Session::has('message'))
				   <div class="alert alert-success alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ Session::get('message') }}</div>
				@endif
				<div class="row">
					<div class="col-sm-12">
						<a href="{{ URL::action('Admin\TileController@getTileCreate') }}" class="btn btn-sm btn-success">Tegel toevoegen</a>
					
						<div class="dropdown" style="float:right;">
							<button style="width:300px;" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
							<?php echo $cn; ?>
							<span class="caret"></span></button>
							<ul class="dropdown-menu" style="width:300px;">
							<?php echo $all; ?>
							<?php foreach($cat_list as $c){ ?>
							
								<li><a href="<?php echo $in . $c['id']; ?>"><?php echo $c['name']; ?></a></li>
							
							<?php } ?>
							</ul>
						</div> 
					</div>
				</div>
				<br/>
					<table class="table table-bordered">
						<thead>
						<tr>
							<th class="cell-small text-center">ID</th>
							<th>Tegel</th>
							<th>Categorie</th>
							<th class="cell-small text-center">Prijs</th>
							<th class="cell-small text-center">Afbeeldingen</th>
							<th class="cell-small text-center">Acties</th>
						</tr>
						</thead>

						<tbody>

						<?php foreach($tiles as $tile){ ?>

							<tr>
								<td class="text-center"><?php echo "<i>" . $tile->id . "</i>"; ?></td>
								<td><?php echo "<strong>" . $tile->name . "</strong>"; ?></td>
								<td><?php echo $tile->cid; ?></td>
								<td class="text-center"><?php echo $tile->price; ?></td>
								<td class="text-center">
									<div class="btn-group">
										<a href="tiles/images/<?php echo $tile->id; ?>" class="btn btn-xs btn-info"><i class="fa fa-picture-o"></i></a>
									</div>
								</td>
								
								<td class="text-center">
									<div class="btn-group">
										<a href="tiles/edit/<?php echo $tile->id; ?>" data-toggle="tooltip" title="Bewerken" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
										<a href="tiles/delete/<?php echo $tile->id; ?>" data-toggle="tooltip" title="Verwijderen" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
									</div>
								</td>
							</tr>

						<?php } ?>

						</tbody>
					</table>
				
            </div>
        </div>
@endsection