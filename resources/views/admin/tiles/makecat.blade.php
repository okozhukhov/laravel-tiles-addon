@extends('admin.layouts.general')

@section('content')
    <h3 class="page-header page-header-top">Tegels Categorieën Toevoegen</h1>
        <div class="row">
            <div class="col-md-12">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>

					{!! Form::open(array('route' => 'categoryAdd', 'class' => 'form', 'enctype' => 'multipart/form-data')) !!}

					<div class="form-group">
						{!! Form::label('Categorie naam') !!}
						{!! Form::text('name', null, 
							array('required', 
								  'class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('Categorie korte beschrijving') !!}
						{!! Form::textarea('description', null, 
							array('class' => 'form-control input-sm ckeditor')) !!}
					</div>
					<div class="form-group" style="float:left;width:49%;">
						{!! Form::label('Categorie pictogram') !!}
						{!! Form::file('caticon') !!}
					</div>
					<div class="form-group" style="float:right;width:49%;">
						{!! Form::label('Bovenliggende categorie') !!}
						<select style="width:70%;padding:7px 12px;" id="csel" name="parent">
						<option value="" selected>Geen</option>
						<?php foreach($cats as $c){ ?>
							<option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
						<?php } ?>
						</select> 
					</div>
					<div style="clear:both;"></div>
					<div class="form-group">
						{!! Form::submit('Toevoegen', 
						  array('class'=>'btn btn-primary')) !!}
					</div>
					{!! Form::close() !!}
					
					
            </div>
        </div>
@endsection