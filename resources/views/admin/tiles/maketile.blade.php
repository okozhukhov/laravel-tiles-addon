@extends('admin.layouts.general')

@section('content')
    <h3 class="page-header page-header-top">Tegels Toevoegen</h1>
        <div class="row">
            <div class="col-md-12">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
					
					

					{!! Form::open(array('route' => 'tileAdd', 'class' => 'form', 'enctype' => 'multipart/form-data')) !!}

					<div class="form-group" style="width:79%;float:left;">
						{!! Form::label('Tegel naam') !!}
						{!! Form::text('name', null, 
							array('required', 
								  'class'=>'form-control')) !!}
					</div>
					<div class="form-group" style="width:19%;float:right;">
						{!! Form::label('Tegel prijs') !!}
						{!! Form::number('price', '0', 
							array('required', 
								  'class'=>'form-control')) !!}
					</div>
					<div style="clear:both;"></div>
					<div class="form-group" style="width:59%;float:left;">
						{!! Form::label('Tegel categorie ( moet categorie-ID zijn en gescheiden door een komma )') !!}
						{!! Form::text('category', null, 
							array('required', 
								  'class'=>'form-control', 'id'=>'vcat')) !!}
					</div>
					<div class="form-group" style="width:39%;float:right;">
					{!! Form::label('Selecteer een categorie om toe te voegen') !!}
						<select style="width:70%;padding:7px 12px;" id="csel">
						<?php foreach($cat_list as $c){ ?>
							<option value="<?php echo $c['id']; ?>" <?php if($c['id'] == "0"){ echo "selected";} ?>><?php echo $c['name']; ?></option>
						<?php } ?>
						</select> 
						<a id="cbtn" style="width:20%;padding:7px;display:block;background:#df4a58;color:#fff;float:right;text-align:center;cursor:pointer;">Toevoegen</a>
					</div>
					<div style="clear:both;margin-bottom:10px;" class="result"></div>
					<div class="form-group">
						{!! Form::label('Tegel korte beschrijving') !!}
						{!! Form::textarea('notes', null, 
							array( 'class' => 'form-control input-sm ckeditor')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('Tegel hoofdbeschrijving') !!}
						{!! Form::textarea('description', null, 
							array( 'class' => 'form-control input-sm ckeditor')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('Tegel beeld ( Later kan er meer worden toegevoegd )') !!}
						{!! Form::file('image') !!}
					</div>
					<div class="form-group">
						{!! Form::submit('Toevoegen', 
						  array('class'=>'btn btn-primary')) !!}
					</div>
					{!! Form::close() !!}
					
					
            </div>
        </div>
		<script>
			$("#cbtn").click(function(){
				//alert("The paragraph was clicked.");
				
				var vsel = $('#csel').val();
				var vcat = $('#vcat').val();
				
				if(vcat == ""){
					$('#vcat').val(vsel);
				}else{
					$('#vcat').val(vcat + "," + vsel);
				}
				
				$.get( "/admin/ajxcat/" + $('#vcat').val(), function( data ) {
				  $( ".result" ).html( data );
				});
				
			});
			
			$("#vcat").change(function() {
				$.get( "/admin/ajxcat/" + $('#vcat').val(), function( data ) {
				  $( ".result" ).html( data );
				});
			});
			
			$( document ).ready(function() {
				$.get( "/admin/ajxcat/" + $('#vcat').val(), function( data ) {
				  $( ".result" ).html( data );
				});
			});
		</script>
@endsection