@extends('layouts.sub', ['text' => 'test'])

@section('content')
<h1><?php echo $act_name; ?></h1>
<span class="cweep"><?php echo $desc; ?></span>
<?php
foreach($tiles as $tile){
if($tile->images == ""){
	$icon = "http://www.eurotegels.nl/templates/frontend/images/offer-img.png";
}
else
{
	$img = explode("|", $tile->images);
	$icon = "/source/offline/images/tiles/" . $img[0];
}

$url_name = str_replace(" ","-",$tile->name);
?>

<a href="/tegel/<?php echo $url_name; ?>"><div class="mcat"><img src="<?php echo $icon; ?>"><p class="item"><?php echo $tile->name; ?></p></div></a>

<?php } ?>
@endsection